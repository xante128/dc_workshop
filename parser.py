import sys
import json
# import urllib.parse

from urlparse import urlparse
import os
import errno


all_ranges = []
global_text = ''

def main(data_folder):
    coverage_files = [os.path.join(data_folder, f) for f in os.listdir(data_folder) if (os.path.isfile(os.path.join(data_folder, f)) and '.json' in f)]
    ref_text = ''
    for file in sorted(coverage_files):
        source_file = os.path.basename(file)
        data = read_coverage(file)
        for id in range(len(data)):
            # Only parse "scripts.js" coverage
            if ('scripts.js?version=1' in data[id]['url'] ):
                cov =  parse_file_coverage(data[id])
                ref_text = cov['text']
                out_str = "{0:30} | {1} | {2:<05.2f}".format(source_file, 'script.js', cov['coverage'])
                print(out_str)
                
    unq_all_ranges = get_unique_ranges(all_ranges)
    cov = compute_coverage(unq_all_ranges, ref_text)
    print('Combined coverage: \n{:<05.2f}'.format(cov['coverage']))


def read_coverage(coverage):
    with open(coverage) as f:
        data = json.load(f)
    return data


def to_filename(url):
    path = urlparse(url).path
    filename = os.path.basename(path)
    return filename

def compute_coverage(ranges, text):
    ret =  { }
    used_bytes = 0
    unused_bytes = 0
    total_bytes = len(text)
    
    for range in ranges:
        used_bytes = used_bytes + range['end'] - range['start']
    coverage = (float(used_bytes) / total_bytes)*100
    ret["used_bytes"] = used_bytes
    ret['unused_bytes'] = unused_bytes
    ret['total_bytes'] = total_bytes
    ret['coverage'] = coverage
    ret['text'] = text
    return ret

def is_contained(range1, range2):
    if (range1['start'] >= range2['start'] and  range1['end'] <= range2['end']):
        return True
    return False

def get_unique_ranges(ranges):
    unq_ranges = []
    for i, r1 in enumerate(ranges):
        r1_is_contained = False
        for j, r2 in enumerate(ranges):
            if (i != j):
                if is_contained(r1, r2):
                    r1_is_contained = True
                    break
        if (not r1_is_contained):
            unq_ranges.append(r1)
    return unq_ranges

def parse_file_coverage(file_coverage):
    filename = to_filename(file_coverage['url'])
    if not filename:
        return
    ranges = file_coverage['ranges']
    text = file_coverage['text']
    unq_ranges = get_unique_ranges(ranges)

    # print('Creating: {0}'.format(filename))
    for range in unq_ranges:
        add_to_global = True
        for r in all_ranges:
            if (r['start'] == range['start'] and  r['end']==range['end']):
                add_to_global = False
        if add_to_global:
            all_ranges.append(range)
    # global_text = text

    rez = compute_coverage(unq_ranges, text)
    return rez


if __name__ == '__main__':
    if (len(sys.argv) < 2):
        print('Coverage folder not provided. Please provide folder containing V8 .json coverage')
    main(sys.argv[1])
