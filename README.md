A simple script to parse chrome DevTools coverage results

# Usage
Run ``python parser.py <Directory containing the downloaded V8 coverage files>``
or 
Run ``./run.sh`` - script called with ``./data`` directory.

The script prints the coverage percentage for the coverage files in the directory, as well as their combined coverage.